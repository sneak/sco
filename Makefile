VERSION := $(shell git rev-parse --short HEAD)
COMMIT := $(shell git log --pretty=format:"%H" -1)
BUILDTIME := $(shell date -u '+%Y-%m-%dT%H:%M:%SZ')
BUILDTIMEFILENAME := $(shell date -u '+%Y%m%d-%H%M%SZ')
BUILDTIMETAG := $(shell date -u '+%Y%m%d%H%M%S')
BUILDUSER := $(shell whoami)
BUILDHOST := $(shell hostname -s)
BUILDARCH := $(shell uname -m)

FN := sco
IMAGENAME := sneak/$(FN)

UNAME_S := $(shell uname -s)

GOLDFLAGS += -X main.Version=$(VERSION)
GOLDFLAGS += -X main.Commit=$(COMMIT)
GOLDFLAGS += -X main.Buildarch=$(BUILDARCH)

# osx can't statically link apparently?!
ifeq ($(UNAME_S),Darwin)
	GOFLAGS := -ldflags "$(GOLDFLAGS)"
endif

ifneq ($(UNAME_S),Darwin)
	GOFLAGS = -ldflags "-linkmode external -extldflags -static $(GOLDFLAGS)"
endif

default: fmt

fmt:
	go fmt ./...
	goimports -l -w .

docker-build:
	docker build -t $(IMAGENAME) .

run: go-get
	cd cmd/$(FN) && go run

build: ./$(FN)

go-get:
	cd cmd/$(FN) && go get -v

vet:
	go vet ./...
	bash -c 'test -z "$$(gofmt -l .)"'

./$(FN): */*.go cmd/*/*.go go-get vet
	cd cmd/$(FN) && go build -o ../../$(FN) $(GOFLAGS) .
