package main

import (
	"os"

	"git.eeqj.de/sneak/sco/bot"
)

var Version string
var Commit string

var Buildarch string

func main() {
	mybot := bot.New(func(b *bot.Bot) {
		b.BotName = "lsvsco"
		b.Version = Version
		b.Commit = Commit
		b.Buildarch = Buildarch
		b.APIURL = os.Getenv("SCO_API_URL")
		b.TeamName = os.Getenv("SCO_TEAM_NAME")
		b.WebsocketURL = os.Getenv("SCO_WEBSOCKET_URL")
		b.DebuggingChannelName = os.Getenv("SCO_DEBUG_CHANNEL")
		b.AccountEmail = os.Getenv("SCO_ACCOUNT_EMAIL")
		b.AccountPassword = os.Getenv("SCO_ACCOUNT_PASSWORD")
		b.AccountUsername = os.Getenv("SCO_ACCOUNT_USERNAME")
		b.AccountFirstname = "LSV"
		b.AccountLastname = "Serious Callers Only"
	})
	mybot.Main()
}
