module git.eeqj.de/sneak/sco

go 1.15

require (
	github.com/kr/pretty v0.1.0
	github.com/mattermost/mattermost-server/v5 v5.26.2
	github.com/mattn/go-isatty v0.0.12
	github.com/rs/zerolog v1.19.0
)
