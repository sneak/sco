# LSV Serious Callers Only (sneak/sco)

Mattermost bot.

# Environment Variables

## for bot comms to mattermost server

* `SCO_API_URL`
* `SCO_WEBSOCKET_URL`
* `SCO_DEBUG_CHANNEL`
* `SCO_ACCOUNT_EMAIL`
* `SCO_ACCOUNT_PASSWORD`
* `SCO_TEAM_NAME`
* `SCO_ACCOUNT_USERNAME`

## remote stuff

* `METAR_API_TOKEN` for https://avwx.rest
* `AIRNOW_API_KEY` for http://www.airnowapi.org

# status

[![Build Status](https://drone.datavi.be/api/badges/sneak/sco/status.svg)](https://drone.datavi.be/sneak/sco)

