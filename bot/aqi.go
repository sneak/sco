package bot

//import "github.com/kr/pretty"
import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"regexp"
	"time"

	"github.com/rs/zerolog/log"
)

type AQIResponse struct {
	DateObserved  string  `json:"DateObserved"`
	HourObserved  int     `json:"HourObserved"`
	LocalTimeZone string  `json:"LocalTimeZone"`
	ReportingArea string  `json:"ReportingArea"`
	StateCode     string  `json:"StateCode"`
	Latitude      float64 `json:"Latitude"`
	Longitude     float64 `json:"Longitude"`
	ParameterName string  `json:"ParameterName"`
	AQI           int     `json:"AQI"`
	Category      struct {
		Number int    `json:"Number"`
		Name   string `json:"Name"`
	} `json:"Category"`
}

func formatAQIResponse(input *[]AQIResponse, zip string) string {
	dinput := *input
	in := dinput[0]
	when := fmt.Sprintf("%s at %d:00 %s", in.DateObserved, in.HourObserved, in.LocalTimeZone)
	bytes, err := json.Marshal(input)
	if err != nil {
		panic(err)
	}
	formatted := string(bytes)
	out := fmt.Sprintf("# AQI for %s as of %s:\n %s", zip, when, formatted)
	return out
}

func (b *Bot) HandleAirQualityRequest(channelid string, postid string, message string) {

	// we are using a very bare image with no CA cert bundle
	// actually if you docker bind mount the ca cert bundle in the right
	// place, golang will find it and use it.
	//http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: true}

	log.Info().Msgf("aqi request received: `%s`", message)

	r := regexp.MustCompile(`aqi\s+([0-9]{5})`)
	matches := r.FindStringSubmatch(message)
	if len(matches) < 2 {
		b.SendMsgToChannel("error, sorry", postid, channelid)
	}

	zip := matches[1]

	apikey := os.Getenv("AIRNOW_API_KEY")
	url := fmt.Sprintf("http://www.airnowapi.org/aq/observation/zipCode/current/?format=application/json&zipCode=%s&distance=25&API_KEY=%s", zip, apikey)

	log.Info().Msgf("calculated url: `%s`", url)

	client := http.Client{
		Timeout: 5 * time.Second,
	}
	req, err := http.NewRequest("GET", url, nil)
	resp, err := client.Do(req)

	if err != nil {
		b.SendMsgToChannel(fmt.Sprintf("aqi fetch error: %s", err), postid, channelid)
		return
	}

	if resp.StatusCode != http.StatusOK {
		b.SendMsgToChannel(fmt.Sprintf("aqi fetch error: http status %d", resp.StatusCode), postid, channelid)
		return
	}

	data, _ := ioutil.ReadAll(resp.Body)

	var parsedAQIResponse []AQIResponse

	log.Info().Msgf("aqi %s: %s", zip, data)

	err = nil
	err = json.Unmarshal([]byte(data), &parsedAQIResponse)

	if err != nil {
		b.SendMsgToChannel("error deserializing AQI data", postid, channelid)
		return
	}

	msg := formatAQIResponse(&parsedAQIResponse, zip)
	b.SendMsgToChannel(msg, postid, channelid)
}
