FROM golang:1.15 as builder

RUN mkdir /build
ADD . /build/
WORKDIR /build
RUN make build


FROM scratch

COPY --from=builder /build/sco /app/main
COPY --from=builder /go /goarchive

WORKDIR /app
CMD ["./main"]
